//
//  CNNotificationView.m
//
//  Created by Bryce Dougherty on 5/8/14.
//  Copyright (c) 2014 Bryce Dougherty. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CNNotificationView;
@protocol CNNotificationViewDelegate;

extern NSString *kCNNotificationViewTapReceivedNotification;

typedef void (^CNNotificationSimpleAction)(CNNotificationView * view);

@interface CNNotificationView : UIView

@property (nonatomic, strong) IBOutlet UILabel *textLabel;
@property (nonatomic, strong) IBOutlet UILabel *detailTextLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, assign) IBOutlet id<CNNotificationViewDelegate> delegate;

@property (nonatomic) NSTimeInterval duration;

+ (CNNotificationView *) notifyWithText:(NSString*)text
                                 detail:(NSString*)detail
                                  image:(UIImage*)image
                            andDuration:(NSTimeInterval)duration;

+ (CNNotificationView *) notifyWithText:(NSString*)text
                                 detail:(NSString*)detail
                            andDuration:(NSTimeInterval)duration;

+ (CNNotificationView *) notifyWithText:(NSString*)text
                              andDetail:(NSString*)detail;

+ (CNNotificationView *) notifyWithText:(NSString*)text
                                 detail:(NSString*)detail
                                  image:(UIImage*)image
                               duration:(NSTimeInterval)duration
                          andTouchBlock:(CNNotificationSimpleAction)block;

+ (CNNotificationView *) notifyWithText:(NSString*)text
                                 detail:(NSString*)detail
                               duration:(NSTimeInterval)duration
                          andTouchBlock:(CNNotificationSimpleAction)block;

+ (CNNotificationView *) notifyWithText:(NSString*)text
                                 detail:(NSString*)detail
                          andTouchBlock:(CNNotificationSimpleAction)block;

+ (CNNotificationView *) notifyWithText:(NSString*)text
                                 detail:(NSString*)detail
                                  image:(UIImage*)image
                               duration:(NSTimeInterval)duration
                                   type:(NSString *)type
                          andTouchBlock:(CNNotificationSimpleAction)block;


+ (void)registerNibNameOrClass:(id)nibNameOrClass
        forNotificationsOfType:(NSString *)type;
+ (void) showNextNotification;


@end

@protocol CNNotificationViewDelegate <NSObject>

@optional
- (void)didTapOnNotificationView:(CNNotificationView *)notificationView;

@end
