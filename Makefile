TARGET = iPhone:8.0:5.0
ARCHS = armv7 arm64

THEOS_DEVICE_IP?=localhost
THEOS_DEVICE_PORT?=2222
ADDITIONAL_OBJCFLAGS = -fobjc-arc
include theos/makefiles/common.mk

TWEAK_NAME = ClassicNotifications
ClassicNotifications_FILES = Tweak.xm CNNotificationView.m OBGradientView.m
ClassicNotifications_FRAMEWORKS = UIKit CoreGraphics QuartzCore

include $(THEOS_MAKE_PATH)/tweak.mk
