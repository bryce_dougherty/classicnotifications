/* How to Hook with Logos
Hooks are written with syntax similar to that of an Objective-C @implementation.
You don't need to #include <substrate.h>, it will be done automatically, as will
the generation of a class list and an automatic constructor.

%hook ClassName

// Hooking a class method
+ (id)sharedInstance {
	return %orig;
}

// Hooking an instance method with an argument.
- (void)messageName:(int)argument {
	%log; // Write a message about this call, including its class, name and arguments, to the system log.

	%orig; // Call through to the original function with its original arguments.
	%orig(nil); // Call through to the original function with a custom argument.

	// If you use %orig(), you MUST supply all arguments (except for self and _cmd, the automatically generated ones.)
}

// Hooking an instance method with no arguments.
- (id)noArguments {
	%log;
	id awesome = %orig;
	[awesome doSomethingElse];

	return awesome;
}

// Always make sure you clean up after yourself; Not doing so could have grave consequences!
%end
*/
#import "Tweak.h"
#import "CNNotificationView.h"
#import "CMNavBarNotificationView.h"

@interface SBBulletinBannerItem
+ (id)itemWithBulletin:(id)fp8 andObserver:(id)fp12;
+ (id)itemWithSeedBulletin:(id)fp8 additionalBulletins:(id)fp12 andObserver:(id)fp16;
- (id)attachmentImage;
- (id)attachmentText;
- (id)iconImage;
- (id)sourceDate;
- (id)message;
- (id)title;
- (id)_appName;
- (id)action;
- (id)seedBulletin;
@end

@interface BBBulletin
- (id)bulletinID;
-(id)publisherBulletinID;
-(id)sectionID;
@end

//DTAlertView *currentAlert = nil;
@implementation ActionSheetThing

+(UIWindow *)getWindow {
	return window;
}

@end



/*%hook UIAlertView

- (id)initWithTitle:(id)arg1 message:(id)arg2 delegate:(id)arg3 cancelButtonTitle:(id)arg4 otherButtonTitles:(id)arg5 {
	
	
    
	return [DTAlertView alertViewWithTitle:arg1 message:arg2 delegate:arg3 cancelButtonTitle:arg4 positiveButtonTitle:arg5];
}
*/
/*-(void)show {
	[currentAlert show];
}
*/


//%end
%hook SBBulletinBannerController
- (void)observer:(id)fp8 addBulletin:(id)fp12 forFeed:(unsigned int)fp16 {
	//NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	//NSString *message = @"blank";
	//= [SBBulletinBannerItem itemWithBulletin:fp12 andObserver:fp8];
	//SBBulletinBannerItem *bannerItem = [%c(SBBulletinBannerItem) itemWithBulletin:fp8 andObserver:fp8];
	SBBulletinBannerItem *bannerItem = [%c(SBBulletinBannerItem) itemWithBulletin:fp12 andObserver:fp8];
	NSLog(@"Action: %@", [bannerItem action]);
	NSLog(@"App: %@", [bannerItem _appName]);
	NSLog(@"Seed: %@", [bannerItem seedBulletin]);
	BBBulletin *seedBulletin = [bannerItem seedBulletin];
	NSLog(@"BulletinID: %@", [seedBulletin bulletinID]);
	NSLog(@"PublisherBulletinID: %@", [seedBulletin publisherBulletinID]);
	NSLog(@"Section ID: %@", [seedBulletin sectionID]);
	//SBApplicationIcon *theicon = [[%c(SBIconModel) sharedInstance] applicationIconForDisplayIdentifier:[seedBulletin sectionID]];
	//UIImage *appIcon = [[[%c(SBIconModel) sharedInstance] applicationIconForDisplayIdentifier:[seedBulletin sectionID]] getUnmaskedIconImage:0];
	//UIImage *appIcon = [UIImage _applicationIconImageForBundleIdentifier:[seedBulletin sectionID] format:0 scale:[UIScreen mainScreen].scale];
	
	
	
	//SBApplication *application = [[%c(SBApplicationController) sharedInstance]applicationWithDisplayIdentifier:[seedBulletin sectionID]];
	//NSBundle *applicationBundle = [NSBundle bundleWithPath:application.path];
	
	//NSString *filePath = nil;
	
	UIImage *appIcon = [bannerItem iconImage];
	
	[CNNotificationView notifyWithText:[bannerItem title]
								detail:[bannerItem message]
								 image:appIcon
							  duration:2.0
						 andTouchBlock:[[bannerItem action] copy]
	 ];

	
	//UIImage *appIcon = [UIImage imageNamed: [[[[NSBundle bundleWithIdentifier:[seedBulletin publisherBulletinID]] infoDictionary] objectForKey:@"CFBundleIconFiles"] objectAtIndex:0]];
	
	NSLog(@"The Method was called!!!!!!!!!!!!!!1!!!");
	
	
	
	
	
	
	//Class bulletinRequest = objc_getClass("BBBulletinRequest");
	
	
	//BBBulletinRequest *request = [[bulletinRequest alloc] init];
	
	//return %orig(nil, request, 2);
	return %orig;
}
%end

%hook SBBannerController
- (void)_presentBannerForContext:(id)fp8 reason:(int)fp12 {
	
}

%end

